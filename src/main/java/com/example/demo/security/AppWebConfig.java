package com.example.demo.security;

import com.example.demo.auth.ApplicationUserService;
import com.example.demo.jwt.JwtTokenVerifier;
import com.example.demo.jwt.JwtUserNameAndPasswordAuthFilter;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.example.demo.security.AppWebConfig.PERMISSION.COURSE_READ;
import static com.example.demo.security.AppWebConfig.PERMISSION.COURSE_WRITE;
import static com.example.demo.security.AppWebConfig.PERMISSION.STUDENT_READ;
import static com.example.demo.security.AppWebConfig.PERMISSION.STUDENT_WRITE;
import static com.example.demo.security.AppWebConfig.ROLES.ADMIN;
import static com.example.demo.security.AppWebConfig.ROLES.ADMINTRAINEE;
import static com.example.demo.security.AppWebConfig.ROLES.STUDENT;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true) // @PreAuthorize-hoz kell.
public class AppWebConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ApplicationUserService applicationUserService;


    // Video https://youtu.be/her_7pa0vrg?list=WL&t=12928
    public enum ROLES {
        STUDENT(Sets.newHashSet()),
        ADMIN(Sets.newHashSet(COURSE_READ, COURSE_WRITE, STUDENT_READ, STUDENT_WRITE)),
        ADMINTRAINEE(Sets.newHashSet(COURSE_READ, STUDENT_READ));

        private final Set<PERMISSION> permissions;

        ROLES(Set<PERMISSION> permissions) {
            this.permissions = permissions;
        }

        public Set<PERMISSION> getPermissions() {
            return permissions;
        }


        public Set<SimpleGrantedAuthority> getGrantedAuthorities() {
            var permissions = getPermissions()
                    .stream()
                    .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                    .collect(Collectors.toSet());
            permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
            return permissions;
        }
    }

    public enum PERMISSION {
        STUDENT_READ("student:read"),
        STUDENT_WRITE("student:write"),
        COURSE_READ("course:read"),
        COURSE_WRITE("course:write");

        public final String permission;

        PERMISSION(String permission) {
            this.permission = permission;
        }

        public String getPermission() {
            return permission;
        }
    }


    // .antMatchers sorrend számit.
  /*  @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                //.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()) // Tokenek gyártása
                .csrf().disable()// Ha be van kapcsolva, Spring egy XSRF Token-t vár a request header-ben.
                .authorizeRequests()// Engedélyezünk kéréseket.
                .antMatchers("/", "index", "/css/*", "/js/*").permitAll() //authorizeRequests után lehet whitelist-elni.
                .antMatchers("/api/**").hasRole(STUDENT.name())
//Ezek ugyanazok mint a @PreAuthorize .antMatchers(HttpMethod.DELETE, "/management/api/**").hasAuthority(COURSE_WRITE.getPermission()) // Jogosultság szintű szűrés
//                .antMatchers(HttpMethod.POST, "/management/api/**").hasAuthority(COURSE_WRITE.getPermission()) // Jogosultság szintű szűrés
//                .antMatchers(HttpMethod.PUT, "/management/api/**").hasAuthority(COURSE_WRITE.getPermission()) // Jogosultság szintű szűrés
//                .antMatchers(HttpMethod.GET, "/management/api/**").hasAnyRole(ADMIN.name(), ADMINTRAINEE.name())
                .anyRequest()  // Minden kérés
                .authenticated()  //Authentikáció mögött
                .and()
                .httpBasic();   // HttpBasic authentikáció mögött. js pop-up, nem form based.
        // Nem lehet logoutolni.
        // User és pass minden request-nél küldődik ennél base64.

    }*/

/*
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "index", "/css/*", "/js/*").permitAll()
                .antMatchers("/api/**").hasRole(STUDENT.name())
                .antMatchers(HttpMethod.GET, "/management/api/**").hasAnyRole(ADMIN.name(), ADMINTRAINEE.name())
                .anyRequest()
                .authenticated()
                .and()
                .formLogin() // Form based authentication
                .loginPage("/login").permitAll() //Login page megadása
                .defaultSuccessUrl("/courses", true) // Login utáni redirect
                .and()
                .rememberMe().tokenValiditySeconds((int) TimeUnit.DAYS.toSeconds(21)).key("somethingverysecured") // Session Id tovább él (2 hétig alapértelmezetten) Csinál egy remember-me cookiet. Kell küldeni a login request-ben egy remember-me értéket.
                // Tartalma: username, expiration time, md5 hash mindkét értékkel. 21 napra bővitettük. A key valami érték, amivel ki lehet bontani az md5-öt.
                .and()
                .logout()
                .logoutUrl("/logout")   // erre az endpointra jelentkeztet ki
                .clearAuthentication(true)  // kilépteti a usert
                .invalidateHttpSession(true)  // bezárja a session-t
                .deleteCookies("JSESSIONID", "remember-me") // törli a cookie-kat
                .logoutSuccessUrl("/login"); // logoutkor ide lép
        // Logout alapból GET kérés, ha csrf ki van kapcsolva, legyen POST, ha be van kapcsolva.
        //Form based:
        // User/pass
        // Can logout
        // https recommended
        // Session-t érdemes redis-ben vagy vmi db-ben tárolni.
        // Session alapból 30 percig él.

    }*/
    // JWT filter config.
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // Session nem lesz tárolva a db-ben.
                .and()
                .addFilter(new JwtUserNameAndPasswordAuthFilter(authenticationManager())) // Filter hozzáadása.
                .addFilterAfter(new JwtTokenVerifier(), JwtUserNameAndPasswordAuthFilter.class) // Második filter.
                .authorizeRequests()
                .antMatchers("/", "index", "/css/*", "/js/*").permitAll()
                .antMatchers("/api/**").hasRole(STUDENT.name())
                .antMatchers(HttpMethod.GET, "/management/api/**").hasAnyRole(ADMIN.name(), ADMINTRAINEE.name())
                .anyRequest()
                .authenticated();
    }

    // DAO authenticationProvider bekötése.
    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    // Implementált UserDetailsService bekötése
    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(passwordEncoder);
        provider.setUserDetailsService(applicationUserService);

        return provider;
    }

    /*@Override In-memory user példák, ha nincs implementálva UserDetailsService
    @Bean
    protected UserDetailsService userDetailsService() {
        User.UserBuilder user = User.builder()
                .username("asd")
                .password(passwordEncoder.encode("asd"))
                //.roles(STUDENT.name())// ROLE_STUDENT
                .authorities(STUDENT.getGrantedAuthorities());


        User.UserBuilder user2 = User.builder()
                .username("bsd")
                .password(passwordEncoder.encode("bsd"))
                //.roles(ADMIN.name())// ROLE_ADMIN
                .authorities(ADMIN.getGrantedAuthorities());


        User.UserBuilder user3 = User.builder()
                .username("csd")
                .password(passwordEncoder.encode("csd"))
                //.roles(ADMINTRAINEE.name())// ROLE_ADMINTRAINEE
                .authorities(ADMINTRAINEE.getGrantedAuthorities());

        return new InMemoryUserDetailsManager(
                user.build(),
                user2.build(),
                user3.build()
        );
    }*/

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10);
    }
}
