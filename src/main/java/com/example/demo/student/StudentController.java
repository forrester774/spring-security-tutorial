package com.example.demo.student;

import com.example.demo.security.AppWebConfig;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.Arrays.asList;

@RestController
@RequestMapping("/api/v1/students/")
public class StudentController {

    public static final List<Student> STUDENTS = asList(
            new Student(1, "A"),
            new Student(2, "B"),
            new Student(3, "C"),
            new Student(4, "D")
    );

    @GetMapping(path = "{studentId}")
    public Student getStudent(@PathVariable Integer studentId) {
        return STUDENTS
                .stream()
                .filter(student -> studentId.equals(student.getStudentId()))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Student not found: " + studentId));
    }
}
